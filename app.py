# from flask import Flask
# from flask import render_template
# from flask_login import LoginManager
# from flask_sqlalchemy import SQLAlchemy
# from flask_migrate import Migrate
# from flask_cors import CORS
# import config
#
# app = Flask(__name__)
# # TODO change to match env
# cors = CORS(app, resources={r"/*": {"origins": "*"}})
#
# app.config.from_object(config.DevConfig)
#
# login_manager = LoginManager()
# login_manager.init_app(app)
#
# db = SQLAlchemy(app)
#
# migrate = Migrate(app, db)
# # TODO There has to be a cleaner way to do this...
#
# from todoapp.models.user import User
#
#
# @login_manager.user_loader
# def load_user(user_id):
#     return User.query.get(int(user_id))
#
#
# from todoapp.controllers.todo import todos
#
# app.register_blueprint(todos)
#
#
# @app.route('/')
# def home():
#     return render_template('main.html')
#
#
# if __name__ == '__main__':
#     app.run()
