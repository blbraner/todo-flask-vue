import Vue from 'vue'
import App from './App.vue'
import store from './store'


Vue.component('app', App);

var app = new Vue({
  el: '#app',
  store
})

