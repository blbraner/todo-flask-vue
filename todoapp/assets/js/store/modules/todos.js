import axios from 'axios';

const APP_URL = process.env.APP_URL;

const state = {
    todos: []
};

const getters = {
    allTodos: state => state.todos,
};

const actions = {
    async fetchTodos({commit}) {
        const response = await axios.get(
            `${APP_URL}/todo`
        );

        commit('setTodos', response.data);
    },
    async addTodo({commit}, title) {
        let request = JSON.stringify({"title": title});
        const response = await axios.post(
            `${APP_URL}/todo`,
            request,
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        );


        commit('newTodo', response.data);
    }
,
    async filterTodos({commit}, e) {
        // Get selected number
        const limit = parseInt(
            e.target.options[e.target.options.selectedIndex].innerText
        );

        const response = await axios.get(
            `http://127.0.0.1:5000/todo?limit=${limit}`
);

commit('setTodos', response.data);
},
};

const mutations = {
setTodos: (state, todos) => (state.todos = todos),
newTodo: (state, todo) => state.todos.unshift(todo),
};

export default {
state,
getters,
actions,
mutations
};
