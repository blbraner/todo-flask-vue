from flask import Blueprint, request, abort, jsonify
from todoapp.services.todo_service import TodoService

todo_blueprint = Blueprint('todos', __name__,
                  template_folder='templates')


@todo_blueprint.route('/todo', methods=['POST'])
def create():
    data = request.get_json()
    if data is None:
        abort(400, '{"message":"body can not be empty, please set a title"')
    title = data.get('title', None)
    if title is None:
        abort(400, '{"message":"title must be set"}')
    service = TodoService()
    try:
        id = service.create_todo(title=title)
    except TypeError:
        abort(400, '{"message":"title must be set"}')
    resp = jsonify({"id": id, 'title': title})
    return resp


@todo_blueprint.route('/todo', methods=['GET'])
def show():
    args = request.args
    limit = args.get('limit', 10)
    offset = args.get('offset', 0)
    service = TodoService()
    todos = service.list_todos(limit=limit, offset=offset)
    return jsonify(todos)
