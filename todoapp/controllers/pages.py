from flask import Blueprint, render_template

pages_blueprint = Blueprint('pages', __name__, template_folder='templates')


@pages_blueprint.route('/', methods=['GET'])
def home():
    return render_template('main.html')