from flask_login import UserMixin, LoginManager
from todoapp.models.base_model import db
from todoapp.models.base_model import BaseModel


class User(UserMixin, db.Model, BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(128))
    created_at = db.Column(db.DATETIME)
    updated_at = db.Column(db.DATETIME)
    deleted_at = db.Column(db.DATETIME)

    def __repr__(self):
        return f"<User {self.username}>"



