from flask import current_app
from flask_sqlalchemy import SQLAlchemy
from todoapp.extensions import db


class BaseModel:

    def add_to_session(self):
        db.session.add(self)

    def commit_session(self):
        db.session.commit()
