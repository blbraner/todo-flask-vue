import datetime

from todoapp.models.base_model import db
from todoapp.models.base_model import BaseModel


class Todo(db.Model, BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    created_at = db.Column(db.DATETIME, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DATETIME)
    deleted_at = db.Column(db.DATETIME)
