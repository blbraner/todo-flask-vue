from todoapp.models.todo import Todo


class TodoService:

    def create_todo(self, title: str):
        if title is None:
            raise TypeError("Title needs to be set")
        todo = Todo()
        todo.title = title
        todo.add_to_session()
        todo.commit_session()
        return todo.id

    def list_todos(self, limit=10, offset=0):
        todos = Todo().query.with_entities(Todo.title, Todo.deleted_at) \
            .filter(Todo.deleted_at.is_(None)) \
            .limit(limit) \
            .offset(offset) \
            .all()
        return [t._asdict() for t in todos]
