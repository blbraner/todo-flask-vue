from flask import Flask
from config import ProdConfig
from todoapp.extensions import db, login_manager, migrate, cors
from todoapp.controllers import todo, pages


def create_app(config_object=ProdConfig):
    app = Flask(__name__)
    app.config.from_object(config_object)
    with app.app_context():
        register_extensions(app)
        register_blueprints(app)
    return app


def register_extensions(app):
    """Register Flask extensions."""
    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)
    from todoapp.models.user import User

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))


def register_blueprints(app):
    """Register Flask blueprints."""
    origins = app.config.get('CORS_ORIGIN_WHITELIST', '*')
    cors.init_app(todo.todo_blueprint, origins=origins)
    cors.init_app(pages.pages_blueprint, origins=origins)

    app.register_blueprint(todo.todo_blueprint)
    app.register_blueprint(pages.pages_blueprint)
