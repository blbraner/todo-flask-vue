# Flask Demo TODO App with VueJS and Vuex

### Setup

Setup a virtual environment however you see fit. https://docs.python-guide.org/dev/virtualenvs/

Install requirements `pip install -r requirements.txt`

Setup environment variables
```
export FLASK_APP=/path/to/run_app.py

// If you want to debug
export FLASK_DEBUG=1
```

Setup your database credentials in `config.py`
```    
SQLALCHEMY_DATABASE_URI = f"mysql+mysqlconnector://{username}:{password}@127.0.0.1/{database}"
```

Modify your `APP_URL` in `webpack.config.js` to be your applications url

### Migrate Database

Run the following from the command line
```
flask db init
flask db migrate
flask db upgraade
```