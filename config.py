import os


class Config:
    SQLALCHEMY_DATABASE_URI = "sqlite+pysqlite:///:memory:"
    DEBUG = False
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = "thisissecret"
    CORS_HEADERS = "Content-Type"


class DevConfig(Config):
    DEBUG = True
    # TODO change these from root and get them from env
    username = os.getenv("database_user") or "root"
    password = os.getenv("database_password") or "password"
    database = os.getenv("database_table") or "todo"
    SQLALCHEMY_DATABASE_URI = f"mysql+mysqlconnector://{username}:{password}@127.0.0.1/{database}"


class TestConfig(Config):
    TESTING = True


class ProdConfig(Config):
    pass
