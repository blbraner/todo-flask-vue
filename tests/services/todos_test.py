from tests.test_base import BaseTestCase
from todoapp.services.todo_service import TodoService
import pytest
from todoapp.models.todo import Todo
import datetime
from todoapp.extensions import db


class TodosTest(BaseTestCase):

    def test_todos_can_be_created(self):
        service = TodoService()
        id = service.create_todo(title="Testing Todo")

        # Asserting id is a type in vs being returned as None
        assert type(id) is int

    def test_todos_with_no_title_return_error(self):
        # Simulate title not being sent in json body
        title = None
        service = TodoService()
        with pytest.raises(TypeError):
            service.create_todo(None)

    def test_we_can_get_todos(self):
        todo_1 = Todo(title="Todo 1")
        todo_2 = Todo(title="Todo 2")
        todo_3 = Todo(title="Todo 3", deleted_at=datetime.datetime.utcnow())
        db.session.add(todo_1)
        db.session.add(todo_2)
        db.session.add(todo_3)
        db.session.commit()

        service = TodoService()
        todos = service.list_todos()
        assert len(todos) == 2

    def test_we_can_get_todos_limit(self):
        todo_1 = Todo(title="Todo 1")
        todo_2 = Todo(title="Todo 2")
        todo_3 = Todo(title="Todo 3", deleted_at=datetime.datetime.utcnow())
        db.session.add(todo_1)
        db.session.add(todo_2)
        db.session.add(todo_3)
        db.session.commit()

        service = TodoService()
        todos = service.list_todos(limit=1)
        assert len(todos) == 1

    def test_we_can_get_todos_offset(self):
        todo_1 = Todo(title="Todo 1")
        todo_2 = Todo(title="Todo 2")
        todo_3 = Todo(title="Todo 3", deleted_at=datetime.datetime.utcnow())
        db.session.add(todo_1)
        db.session.add(todo_2)
        db.session.add(todo_3)
        db.session.commit()

        service = TodoService()
        todos = service.list_todos(offset=1)
        # assert we only get 1 back since the last one is deleted
        assert len(todos) == 1
        # assert that the title is Todo2 since that is the second one that was inserted
        assert todos[0].get('title') == "Todo 2"
