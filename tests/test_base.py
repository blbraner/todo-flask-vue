from flask_testing import TestCase
from config import TestConfig
from todoapp.app import create_app
from todoapp.extensions import db

class BaseTestCase(TestCase):

    def create_app(self):
        app = create_app(TestConfig)
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()