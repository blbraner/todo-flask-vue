from flask.helpers import get_debug_flag
from todoapp.app import create_app
from config import DevConfig, ProdConfig, TestConfig

CONFIG = DevConfig if get_debug_flag() else ProdConfig

app = create_app(CONFIG)
